$(document).ready(function() {

	var toolTipContent = function(o) {
		var c = '';
		if (o.attr('data-nameable') == "1") {
			c = c + 'This space is available for a gift of ';
			if (o.attr('data-gift') != "") {
				c = c + '<br /><p>' + o.attr('data-gift') + '</p>';
			}
		}
		return c;
	}

	// force the image to resize as imageMapster conflicts with 
	// Bootstrap's ability to adjust to media query changes
	$(window).resize(_.debounce(function(){
		location.reload( false );
	}, 500));

	$body_venue = $('body.venue');
	if ($body_venue.length > 0) {
		var areas = [];
		$('area[data-nameable=1]').each(function(el, ndx) {
			var $toolTip = $('<div class="popover-content">' + toolTipContent( $(this) ) + '</div>'), fillColor = '728302', strokeColor = '728302';
			areas.push({ 
				key : $(this).attr('data-venue'), 
				fillColor: '728302', 
				fillOpacity: 0.7,
				strokeColor: '728302',
				strokeOpacity: 1.0,
				toolTip: $toolTip
			});
		});
		$('area[data-nameable=0]').each(function(el, ndx) {
			var $toolTip = $('<div class="popover-content">' + toolTipContent( $(this) ) + '</div>');
			areas.push({ 
				key : $(this).attr('data-venue'), 
				fillColor: '0736a4', 
				fillOpacity: 0.7,
				strokeColor: '0736a4',
				strokeOpacity: 1.0,
				toolTip: null
				//toolTip: $toolTip
			});
		});
		// initial venue imagemapster effects/plugins;
		$('.mapster-aware').mapster({
			fillOpacity: 0.5,
			fillColor: '0736a4',
			stroke: true,
			strokeColor: '0736a4',
			strokeWidth: 1,
			strokeOpacity: 0.5,
			scaleMap: true,
			mapKey: 'data-venue',
			showToolTip : true,
			toolTipContainer: '<div class="popover dukestone" role="tooltip"><div class="arrow"></div><h3 class="popover-title">Iron Dukes</h3></div>',
			clickNavigate: true,
			areas: areas,
			wrapClass: true,
			isSelectable: false,
			onMouseover : function(o) {
				var area_key = o.key;
				if (o.selected) {
					$('#' + area_key).popover('toggle');
				}
			},
			onShowToolTip: function(data) {
				$('h3', data.toolTip).html( $(this).attr('title') + '<br /><small>Duke Athletics</small>' );
			}
		});

		$('area').on('click', function(evt) {
			var url = $(this).attr('data-url');
			var $modal = $('#namingSpaceModal');
			$.ajax({
				url: url,
				data:	null,
				cache:	false,
				dataType:	'json',
				success:	function(data, stat, o) {
					$('.modal-title', $modal).html( data.space_level );
					//$('.naming-space-desc', $modal).html( data.space_desc );

					$('strong', '.naming-amount-text').html(data.space_desc).parent().show();

					if (data.room_mockups.length > 0) {
						var slideTo = 0;
						for (var i=0; i < data.room_mockups.length; i++) {
							console.log(data.room_mockups[i].image_url);
							$slideItem = $('<li data-target="#roomImageCarousel" data-slide-to="' + slideTo + '"></li>');
							$('.carousel-indicators', '#roomImageCarousel').append($slideItem);
							slideTo++;

							$item = $('<div class="item"><img src="' + data.room_mockups[i].image_url + '" /></div>');
							$('.carousel-inner', '#roomImageCarousel').append($item);
						}
						$('.item:first', '#roomImageCarousel').addClass('active');
						$('.naming-space-desc').show();
						$('.carousel-indicators > li:first', '#roomImageCarousel').addClass('active');
					} else {
						$('.naming-space-desc').hide();
					}

					// wait for the carousel images to load first..
					$('.carousel-inner', '#roomImageCarousel').imagesLoaded( function() {
						
						$('#roomImageCarousel').carousel();

						// Configure the MODAL for showtime
						$modal.on('hidden.bs.modal', function(){
							$('.carousel-indicators', '#roomImageCarousel').html('');
							$('.carousel-inner', '#roomImageCarousel').html('');
							$('.naming-amount-text', '#roomImageCarousel').html('');
							$('#roomImageCarousel').carousel('pause');
						}).modal('show');
					});
				} 
			});
			/*
			$.get(url, null, function(data, stat, o) {
				$('.modal-title', $modal).html( data.space_level );
				//$('.naming-space-desc', $modal).html( data.space_desc );

				$('strong', '.naming-amount-text').html(data.space_desc).parent().show();

				if (data.room_mockups.length > 0) {
					var slideTo = 0;
					for (var i=0; i < data.room_mockups.length; i++) {
						$slideItem = $('<li data-target="#roomImageCarousel" data-slide-to="' + slideTo + '"></li>');
						$('.carousel-indicators', '#roomImageCarousel').append($slideItem);
						slideTo++;

						$item = $('<div class="item"><img src="' + data.room_mockups[i].image_url + '" /></div>');
						$('.carousel-inner', '#roomImageCarousel').append($item);
					}
					$('.item:first', '#roomImageCarousel').addClass('active');
					$('.naming-space-desc').show();
					$('.carousel-indicators > li:first', '#roomImageCarousel').addClass('active');
				} else {
					$('.naming-space-desc').hide();
				}

				// wait for the carousel images to load first..
				$('.carousel-inner', '#roomImageCarousel').imagesLoaded( function() {
					// Configure the MODAL for showtime
					$modal.on('hidden.bs.modal', function(){
						$('.carousel-indicators', '#roomImageCarousel').html('');
						$('.carousel-inner', '#roomImageCarousel').html('');
						$('.naming-amount-text', '#roomImageCarousel').html('');
					}).modal('show');
				});
			}, 'json');
			*/
			return evt.preventDefault();
		});

		$('a.nameablespace,a.nonnameablespace')
			.on('mouseover', function() {
				var k = $(this).attr('data-target-area');
				$('.mapster-aware')
					.mapster('highlight', k);
			})
			.on('mouseout', function() {
				var k = $(this).attr('data-target-area');
				$('.mapster-aware')
					.mapster('highlight', false);
			})
			.on('click', function() {
				var k = $(this).attr('data-target-area');
				$('AREA[data-venue="' + k + '"]').click();
			});
		
		return;
	}

	var areas = [];
	areas.push({key: 'wallace-wade-stadium', toolTip: 'Wallace Wade Stadium'});
	try {
		$('#dukeathcampus').mapster({
			fillOpacity: 0.5,
			fillColor: '0736a4',
			stroke: true,
			strokeColor: '0736a4',
			strokeWidth: 1,
			strokeOpacity: 0.5,
			scaleMap: true,
			mapKey: 'data-venue',
			showToolTip : false,
			clickNavigate: true,
			onMouseover : function(o) {
				var area_key = o.key;
				if (o.selected) {
					$('#' + area_key).popover('toggle');
				}
			}
		});
	} catch(err) {
		console.log(err);
	}

	$('a[data-venue]')
		.on('mouseover', function() {
			var k = $(this).attr('data-venue');
			$('#dukeathcampus')
				.mapster('highlight', k);
		})
		.on('mouseout', function() {
			var k = $(this).attr('data-venue');
			$('#dukeathcampus')
				.mapster('highlight', false);
		});

	// initialize Masonry
	if ($('body').is('.masonry-aware')) {
		var $container = $('.masonry-container');
		$container.imagesLoaded( function () {
		$container.masonry({
			columnWidth: '.venue-item',
			itemSelector: '.venue-item'
		});
		});
	}

	// ---- EDIT ROOM DETAILS ----
	$('#namingAmount', '.edit-room-details').prop('disabled', $('#roomNamemable', '.edit-room-details').is(':checked') );


	// ---- GOOGLE ANALYTICS TRACKING ----- //
	$('[data-ga-category]').on('click', function() {
		var category, label, action;
		category = $(this).attr('data-ga-category');
		action = $(this).attr('data-ga-action');
		if ( $(this).is('[data-ga-label]') ) {
			label = $(this).attr('data-ga-label');
		} else {
			label = $(this).attr('alt');
		}
		if (ga) {
			try {
				ga('send', 'event', category, action, label);
			} catch (err) {
				console.log(err);
			}
		}
	});
});
