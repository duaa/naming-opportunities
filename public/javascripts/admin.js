Dropzone.options.myAwesomeDropzone = {
	init: function() {
		this.on("completemultiple", function(files) { 
			var i = 0; 
			$("#fileUploadAlert").on('hidden.bs.modal', function(e) { location.reload(); } ).modal('show');
		});
	},
	acceptedFiles: "image/*",
	paramName: 'upload_mockup',
	uploadMultiple: true
}
$(document).ready(function() {
	$('a[data-href]').on('click', function(evt) {
		var href = $(this).attr('data-href');
		if (confirm("Remove this mockup?") == true) {
			$.ajax( href, {
				dataType: 'json',
				success: function(data, txtStat, o) {
					debugger;
					if (data.result == 'success') {
						alert('The mockup was successfully removed.');
						location.reload();
					}
				},
				error: function(o, txtStat, errThrown) {
					debugger;
					alert('Error');
				}
			});
		}
	});
});
