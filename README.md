# Duke Athletics Naming Opportunities

This project represents the codebase that presents the athletic facilities "Naming Opportunities" 
website for Duke University Athletics (DUAA), currently hosted at:

https://duaa-naming-01.oit.duke.edu

## History

DUAA approached the Libraries to build a version of DUL Naming Opportunities website.  ITS completed the construction 
of the website in early summer of 2017, and DUAA launched it prior to the start of the 
2017 football season.

## Under the Hood

- PERL [Dancer2](https://metacpan.org/pod/distribution/Dancer2/lib/Dancer2/Manual.pod) application framework
- MySQL (hosted on ITS server)
- Bootstrap 3 UI
- ImageMagick

### Server Architecture

- Application Server:
  - `duaa-naming-01.oit.duke.edu` (production)
  - `duaa-naming-test-01.oit.duke.edu` (development, testing)
- Database Server: `lib-sql-01.oit.duke.edu`