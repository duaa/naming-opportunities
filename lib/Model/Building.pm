package duaanaming::Model;

use Moo;

has db => is => ro => required => 1;

sub get_latest {
	my ( $self ) = @_;

	$self->db->search(
		"buildings",
		where => { active => 1 }, sort => { label => -1 },
	);
}
