package duaanaming::Helpers::ImageUtils;
use strict;
use warnings;

use Exporter qw(import);

our @EXPORT_OK = qw(generated_floorplan_image);
use Image::Magick;
use Data::Dumper;

sub generated_floorplan_image {
	my %args = @_;

	my $size = sprintf('%sx%s', $args{width}, $args{height});
	my $image = Image::Magick->new(size=>$size, magick=>'png');
	open(IMAGE, $args{path_original});
	my $p = $image->Read(file=>\*IMAGE);
	close(IMAGE);

	foreach my $a (@{$args{areas}}) {
		my ($prim, $stroke, $fill);
		if ($a->{shape} eq 'rect') {
			$prim = 'rectangle';
		} else {
			$prim = 'polygon';
		}
		if ($a->{nameable}) {
			$fill = $args{colors}->{nameable};
		} else {
			$fill = $args{colors}->{notnameable};
		}
		$image->Draw(stroke=>$fill, fill=>$fill, primitive=>$prim, points=>$a->{coord}, opacity=>'30');
	}
	$image->Write($args{generated_base} . $args{image_name} . '.png');
	return $args{image_name} . '.png';
}

1;

__END__
