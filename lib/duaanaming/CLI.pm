package duaanaming::CLI;

use strict;
use warnings;
use Carp;
use Data::Dumper;

use Dancer2 appname => 'duaanaming';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Ajax;

use Text::CSV;

prefix '/cli';

get '/load-rooms' => sub {
	my $rooms_rs = schema->resultset('Room');
	my $room_mockups_rs = schema->resultset('RoomMockup');
	my $room_mockup_images_rs = schema->resultset('RoomMockupImage');

	my $csv = Text::CSV->new( { binary => 1, sep_char => "\t" } )
									or croak "Cannot use CSV: " . Text::CSV->error_diag();

	open my $fh, "<:encoding(utf8)", "/srv/duaa/room_mockups.csv" or croak "room_mockups.csv: $!";
	while ( my $row = $csv->getline( $fh ) ) {
		debug "processing building [" . $row->[0] . "; room [" . $row->[1] . "]";
		# only process rows that have a 3rd column
		# representing the room ids
		if ( exists $row->[2] ) {
			# split the 3rd column by commas
			my @room_ids = split ',', $row->[2];
			foreach my $room_id (@room_ids) {
				my $room = $rooms_rs->find( $room_id );

				# create a new room_mockup record
				my $new_mockup = $room_mockups_rs->create({
					label => $row->[1],
					image => $row->[1],
					room_id => $room->id,
					created_at => 'NOW()',
					updated_at => 'NOW()',
				}) or croak "Unable to create room_mockup record.\n";
				debug "created new room_mockup record; id = [" . $new_mockup->id . "]";

				my $new_room_mockup_image = $room_mockup_images_rs->create({
					room_id => $room->id,
					room_mockup_id => $new_mockup->id,
					created_at => 'NOW()',
					updated_at => 'NOW()',
				}) or croak "Unable to create room_mockup_image record.\n";
				debug "created new room_mockup_image record; id = [" . $new_room_mockup_image->id . "]";
			}
		}
	}
	close $fh;
};

1;

__END__
