package duaanaming::admin;

use strict;
use warnings;
use Carp;

use Dancer2 appname => 'duaanaming';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::DUAAWeb::ImageMapParser;
use Dancer2::Core::Request::Upload;
use Dancer2::Plugin::Redis;
use Dancer2::Plugin::Ajax;

use Data::Dumper;
use DateTime;
use Image::Magick;
use Math::Round qw(:all);
use Number::Format qw(:subs);

hook before => sub {
	var admin_page_title => setting('admin_page_title');

	#my $intro_text = redis_get('intro_text') || setting('ui')->{front_page}->{intro_text};
	#debug "intro text = [" . $intro_text . "]";
};

hook before_template_render => sub {
	my $tokens = shift;
	$tokens->{admin_page_title} = vars->{admin_page_title};
};

prefix '/admin';

get '' => sub {
	my @buildings = schema->resultset('Building')
		->search(undef, { order_by => 'label' });

	template 'admin/index', { buildings => \@buildings }, { layout => 'admin' };
};

any ['get','post'] => '/edit-venue-info/:buildingid' => sub {
	my $building_id = route_parameters->get('buildingid');

	my $building = schema->resultset('Building')
		->search( { id => $building_id } )
		->first();

	vars->{admin_page_title} = 'Edit ' . $building->label;

	my $body_params = body_parameters;
	if ($body_params->values) {
		my $building_label = body_parameters->get('building_label');
		my $building_sublabel = body_parameters->get('building_sublabel');
		my $building_active = body_parameters->get('building_active') || 0;

		# update the record unless $building is undef (from the 'first()' call)
		$building->update( { 
			label => $building_label,
			sublabel => $building_sublabel,
			active => $building_active
		} ) unless !defined $building;
	}

	template 'admin/edit-venue-info', { building => $building }, { layout => 'admin' };
};

get '/manage-floorplans/:buildingid' => sub {
	my $building_id = route_parameters->get('buildingid');

	my $building = schema->resultset('Building')
		->search( { id => $building_id } )
		->first();

	vars->{admin_page_title} = $building->label . ' Floorplan(s)';

	my @floorplans = schema->resultset('Floorplan')
		->search({ building_id => $building_id }, { order_by => 'label' });

	template 'admin/manage-floorplans', 
		{ floorplans => \@floorplans, building => $building }, 
		{ layout => 'admin' };
};

post '/upload-imagemap/:buildingid' => sub {
};

any ['get','post'] => '/edit-settings' => sub {
	my $body_params = body_parameters;
	if ($body_params->values) {
		my $new_intro_text = $body_params->get('front_page.intro_text');
		redis_set( 'ui:front_page:intro_text', $new_intro_text );

		my $new_venues_per_group = $body_params->get('front_page.venues_per_group');
		redis_set( 'ui:front_page:venues_groups:per_group', $new_venues_per_group );
	}
	my $intro_text = redis_get('ui:front_page:intro_text') || setting('ui')->{front_page}->{intro_text};
	my $venues_per_group = redis_get( 'ui:front_page:venues_groups:per_group' ) || setting('ui')->{front_page}->{venues_groups}->{per_group};
	my $no_avail_spaces_message = redis_get( 'ui:messages:no_available_spaces' ) || setting('ui')->{messages}->{no_available_spaces};
	my $no_named_spaces_message = redis_get( 'ui:messages:no_named_spaces' ) || setting('ui')->{messages}->{no_named_spaces};
	template 'admin/edit-settings', 
		{ 
			intro_text => $intro_text,
			venues_per_group => $venues_per_group,
			no_avail_spaces_message => $no_avail_spaces_message,
			no_named_spaces_message => $no_named_spaces_message,
		}, 
		{ layout => 'admin' };
};

any ['get','post'] => '/edit-floorplan/:floorplanid' => sub {
	my $floorplan_id = route_parameters->get('floorplanid');

	my $floorplan = schema->resultset('Floorplan')
		->search({ id => $floorplan_id })
		->first();

	my $body_params = body_parameters;
	if ($body_params->values) {
		# save and return to 'manage-floorplans'
		my $dt = DateTime->now;
		my $updated_at = $dt->strftime("%Y-%m-%d %H:%M:%S");

		$floorplan->update({
			label => $body_params->get('label'),
			sublabel => $body_params->get('sublabel'),
			weight => $body_params->get('weight'),
			updated_at => $updated_at,
		});
		redirect '/admin/manage-floorplans/' . $floorplan->building_id;
	}

	my $form_title = $floorplan->label || '(Untitled Floorplan)';
	var admin_page_title => $form_title;

	template 'admin/edit-floorplan',
		{ floorplan => $floorplan },
		{ layout => 'admin' };
};

any ['get','post'] => '/edit-room-details/:roomid' => sub {
	my $room_id = route_parameters->get('roomid') or croak "Unable to get room id.";

	my $room = schema->resultset('Room')
		->search( { id => $room_id }, { '+columns' => { formatted_amount => \['FORMAT(naming_amount, 0)'] }} )
		->first();

	vars->{admin_page_title} = $room->label;

	my $body_params = body_parameters;
	if ($body_params->values) {
		my $room_label = $body_params->get('room_label');
		my $display_title = $body_params->get('display_title');
		my $room_number = $body_params->get('room_number');
		my $naming_amount = $body_params->get('room_naming_amount');
		my $naming_amount_desc = $body_params->get('room_naming_amount_desc');
		my $nameable = $body_params->get('room_nameable') || 0;

		my $dt = DateTime->now;
		my $updated_at = $dt->strftime("%Y-%m-%d %H:%M:%S");
		$room->update({
			label => $room_label,
			display_title => $display_title,
			room_number => $room_number,
			naming_amount => $naming_amount,
			naming_amount_desc => $naming_amount_desc,
			nameable => $nameable,
			naming_opportunity => $nameable,
			updated_at => $updated_at,
		}) unless !defined $room;

		#redirect '/admin/manage-floorplans/' . $room->floorplan->building->id;
		var alert_message => "Your changes have been saved.";
		# implement logging output when data changes
	}

	template 'admin/edit-room-details', { room => $room }, { layout => 'admin' };
};

ajax ['get'] => '/remove-room-mockup/:roommockupid' => sub {
	debug "****** in /remove-room-mockup";
	my $room_mockup_id = route_parameters->get('roommockupid');
	my $room_mockup = schema->resultset('RoomMockup')->find( { id => $room_mockup_id } );
	my $room_mockup_image = schema->resultset('RoomMockupImage')->find( { room_mockup_id => $room_mockup_id } );

	my $rows_affected;
	$rows_affected = $room_mockup_image->delete();
	if ($rows_affected > 0) {
		$rows_affected = $room_mockup->delete();
		if ($rows_affected > 0) {
			return encode_json( { result => 'success', rowDeleted => 1 } );
		}
		return encode_json( {result => 'error', message => 'Unable to remove the mockup (room_mockup failed).'} );
	}
	encode_json( {result => 'error', message => 'Unable to remove the mockup (room_mockup_image failed).'} );
};

post '/upload-room-mockup/:roomid' => sub {
	my $room_id = route_parameters->get('roomid');
	my $upload_path = config->{rooms}->{upload_path};
	my $room = schema->resultset('Room')->find( { id => $room_id } );
	my $room_mockup_rs = schema->resultset('RoomMockup');
	my $room_mockup_img_rs = schema->resultset('RoomMockupImage');
	my $response = {
		room_id => $room_id,
		uploaded => [],
	};

	#debug Dumper(request->uploads);
	my $uploads = request->uploads;
	foreach (values %{$uploads}) {
		debug Dumper($_);
		my $fname = $_->filename;
		$fname =~ s/((\s)+)/_/g;
		$fname =~ s(-|'|\(|\))//g;
	
		my $copy_result = $_->copy_to( $upload_path . '/' . $fname );
		debug sprintf("copy result = [%d]", $copy_result);

		# create a new room mockup record
		my $new_room_mockup = $room_mockup_rs->create({
			label => $fname,
			image => $fname,
			room_id => $room_id
		});
		debug Dumper ($new_room_mockup);

		#...and then create a new room_mockup_image record
		my $new_room_mockup_image = $room_mockup_img_rs->create({
			room_id => $room_id,
			room_mockup_id => $new_room_mockup->id,
		});

		push @{$response->{uploaded}}, sprintf("%s was uploaded successfully.", $fname);
	}

	# get the uploaded file
	#my $upload = upload('upload_mockup');
	#debug Dumper($upload);
	#my $fname = $upload->filename;
	#$fname =~ s/((\s)+)/_/g;
	#$fname =~ s(-|'|\(|\))//g;


	#my $new_image_filename = setting('image_base') . 'room_images/' . $fname;
	#save_resized_image( $upload->tempname, $new_image_filename );

	# resize it, similar to what happens in process_rooms
	to_json($response);
};

sub save_resized_image {
	my ($src, $dest) = @_;

	my $image = Image::Magick->new;
	my ($width, $height, $size, $format) = $image->Ping($src);

	my $resize_pct;
	if ( $width >= 1200 ) {
		my $resize_pct = sprintf( "%d%%", (1200.0 / $width) * 100.0 );
		my $x = $image->ReadImage( $src );
		$image->AdaptiveResize( $resize_pct );
		$x = $image->Write( $dest );
	} else {
		copy( $src, $dest ) or croak "Copy failed: $!\n";
	}
}
1;
