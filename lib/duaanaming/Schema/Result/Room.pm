use utf8;
package duaanaming::Schema::Result::Room;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::Room

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<rooms>

=cut

__PACKAGE__->table("rooms");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 room_number

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 display_title

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 128

=head2 display_weight

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 floorplan_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 naming_opportunity

  data_type: 'tinyint'
  is_nullable: 1

=head2 nameable

  data_type: 'tinyint'
  is_nullable: 1

=head2 naming_amount

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 naming_amount_desc

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 128

=head2 other_amount

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 other_amount_desc

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 128

=head2 pending_sale

  data_type: 'tinyint'
  is_nullable: 1

=head2 carrel

  data_type: 'tinyint'
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "room_number",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "display_title",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 128 },
  "display_weight",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "floorplan_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "naming_opportunity",
  { data_type => "tinyint", is_nullable => 1 },
  "nameable",
  { data_type => "tinyint", is_nullable => 1 },
  "naming_amount",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "naming_amount_desc",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 128 },
  "other_amount",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "other_amount_desc",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 128 },
  "pending_sale",
  { data_type => "tinyint", is_nullable => 1 },
  "carrel",
  { data_type => "tinyint", is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 floorplan

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Floorplan>

=cut

__PACKAGE__->belongs_to(
  "floorplan",
  "duaanaming::Schema::Result::Floorplan",
  { id => "floorplan_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 room_areas

Type: has_many

Related object: L<duaanaming::Schema::Result::RoomArea>

=cut

__PACKAGE__->has_many(
  "room_areas",
  "duaanaming::Schema::Result::RoomArea",
  { "foreign.room_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 room_mockup_images

Type: has_many

Related object: L<duaanaming::Schema::Result::RoomMockupImage>

=cut

__PACKAGE__->has_many(
  "room_mockup_images",
  "duaanaming::Schema::Result::RoomMockupImage",
  { "foreign.room_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 room_mockups

Type: has_many

Related object: L<duaanaming::Schema::Result::RoomMockup>

=cut

__PACKAGE__->has_many(
  "room_mockups",
  "duaanaming::Schema::Result::RoomMockup",
  { "foreign.room_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-02-17 10:49:31
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:DR+hTO+y4456aBs0efcbCg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
