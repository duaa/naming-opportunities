use utf8;
package duaanaming::Schema::Result::FloorArea;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::FloorArea

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<floor_areas>

=cut

__PACKAGE__->table("floor_areas");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 coord

  data_type: 'varchar'
  is_nullable: 1
  size: 750

=head2 shape

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 floorplan_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "coord",
  { data_type => "varchar", is_nullable => 1, size => 750 },
  "shape",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "floorplan_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 floorplan

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Floorplan>

=cut

__PACKAGE__->belongs_to(
  "floorplan",
  "duaanaming::Schema::Result::Floorplan",
  { id => "floorplan_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-27 15:49:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GaOnSp3Y+sApReI2EhJW0w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
