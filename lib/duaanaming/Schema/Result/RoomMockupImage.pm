use utf8;
package duaanaming::Schema::Result::RoomMockupImage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::RoomMockupImage

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<room_mockup_images>

=cut

__PACKAGE__->table("room_mockup_images");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 room_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 room_mockup_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "room_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "room_mockup_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 room

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Room>

=cut

__PACKAGE__->belongs_to(
  "room",
  "duaanaming::Schema::Result::Room",
  { id => "room_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 room_mockup

Type: belongs_to

Related object: L<duaanaming::Schema::Result::RoomMockup>

=cut

__PACKAGE__->belongs_to(
  "room_mockup",
  "duaanaming::Schema::Result::RoomMockup",
  { id => "room_mockup_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-27 15:49:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OlYgUfIkyfDxqRMup0G1oA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
