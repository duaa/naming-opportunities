use utf8;
package duaanaming::Schema::Result::RoomMockup;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::RoomMockup

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<room_mockups>

=cut

__PACKAGE__->table("room_mockups");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 image

  data_type: 'varchar'
  is_nullable: 1
  size: 750

=head2 room_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 display_weight

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "image",
  { data_type => "varchar", is_nullable => 1, size => 750 },
  "room_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "display_weight",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "0000-00-00 00:00:00",
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 room

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Room>

=cut

__PACKAGE__->belongs_to(
  "room",
  "duaanaming::Schema::Result::Room",
  { id => "room_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 room_mockup_images

Type: has_many

Related object: L<duaanaming::Schema::Result::RoomMockupImage>

=cut

__PACKAGE__->has_many(
  "room_mockup_images",
  "duaanaming::Schema::Result::RoomMockupImage",
  { "foreign.room_mockup_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-01 14:28:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YjtIox07Q7m5IEo3cXmqyg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
