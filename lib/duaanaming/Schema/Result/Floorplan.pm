use utf8;
package duaanaming::Schema::Result::Floorplan;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::Floorplan

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<floorplans>

=cut

__PACKAGE__->table("floorplans");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 sublabel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 building_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 weight

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 is_default

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 floorplan_map_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "sublabel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "building_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "weight",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "is_default",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "floorplan_map_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 building

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Building>

=cut

__PACKAGE__->belongs_to(
  "building",
  "duaanaming::Schema::Result::Building",
  { id => "building_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 floor_areas

Type: has_many

Related object: L<duaanaming::Schema::Result::FloorArea>

=cut

__PACKAGE__->has_many(
  "floor_areas",
  "duaanaming::Schema::Result::FloorArea",
  { "foreign.floorplan_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 floorplan_map

Type: belongs_to

Related object: L<duaanaming::Schema::Result::FloorplanMap>

=cut

__PACKAGE__->belongs_to(
  "floorplan_map",
  "duaanaming::Schema::Result::FloorplanMap",
  { id => "floorplan_map_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);

=head2 rooms

Type: has_many

Related object: L<duaanaming::Schema::Result::Room>

=cut

__PACKAGE__->has_many(
  "rooms",
  "duaanaming::Schema::Result::Room",
  { "foreign.floorplan_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-04-17 10:44:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iDD/Q5kPxTen/TR9aGk6BQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
