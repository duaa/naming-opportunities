package duaanaming;

use strict;
use warnings;
use Carp;

use Dancer2;
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::DUAAWeb::Common;
use duaanaming::venue;
use duaanaming::CLI;
use duaanaming::admin;

use Data::Dumper;
use Number::Format qw(:subs);

our $VERSION = '0.1';

## hooks
#  =================================
hook before => sub {
	var breadcrumbs => [];
	var body_classes => [];
	var extracss => [];
	var extrajs => [];
	var include_js_template => [];
	var include_css_template => [];

	var no_breadcrumbs => 0;

	# find a way to load overriden config variables (settings) from database
};

hook before_template_render => sub {
	my $tokens = shift;
	$tokens->{body_classes} = vars->{body_classes};

	unshift @{vars->{breadcrumbs}}, { title => 'Naming Opportunities', url => '/', active => 0 };
	$tokens->{breadcrumbs} = vars->{breadcrumbs};

	if (vars->{venues_group_css_class}) {
		$tokens->{venues_group_css_class} = vars->{venues_group_css_class};
	}
	if (vars->{venues_groups}) {
		$tokens->{venues_groups} = vars->{venues_groups};
	}
	$tokens->{extracss} = vars->{extracss};
	$tokens->{extrajs} = vars->{extrajs};
	$tokens->{no_breadcrumbs} = vars->{no_breadcrumbs};
	$tokens->{include_js_template} = vars->{include_js_template};
	$tokens->{include_css_template} = vars->{include_css_template};
};

prefix undef;

get '/' => sub {
	# instead of loading the buildings at first,
	# display an overview of the athletics grounds
	# (similar to http://buildingforchampions.com)
	
	#my $venues = schema->resultset('Floorplan')
	#	->search(undef, {
	#		select => [
	#			{ distinct => 'building_id' },
	#			'building.label',
	#			'building.name',
	#		],
	#		as => [qw/
	#			building_id
	#			venue_label
	#			venue_name
	#		/],
	#		join => 'building',
	#		order_by => [ { -desc => 'building.weight' }, 'building.label']
	#	});

	my $use_mod_layout = query_parameters->get('layout2') || 0;

	my $venues_rs = schema->resultset('Building')->search(undef, {
		group_by => [qw/ name /],
		join => 'floorplans',
		order_by => [ { -desc => 'me.weight' }, 'me.label']
	});
	my @venues = $venues_rs->all();

	# load the 'default floorplan' (floorplans where is_default = 1)
	# or get the first floorplan (based on alphabet sort order)
	grep {
		my $default_floorplan_rs = $_->search_related('floorplans', { is_default => 1 });
		my $default_floorplan = $default_floorplan_rs->first();
		if ( $default_floorplan ) {
			$_->{first_floorplan} = $default_floorplan;
		} else {
			my $sorted_floorplans = $_->search_related('floorplans', undef, { order_by => [ { -asc => 'weight' }, 'label' ] });
			$_->{first_floorplan} = $sorted_floorplans->first();
		}
	} @venues;

	#debug "count of venues = " . scalar @venues;

	vars->{no_breadcrumbs} = 1;
	push @{vars->{extrajs}}, { src => request->uri_base . '/javascripts/jquery.imagemapster.min.js' };

	my $template = $use_mod_layout ? 'index2' : 'index';
	my @venues_groups = ();
	my $current_group = undef;
	my $group_count = 0;
	my $group_max = setting('ui')->{front_page}->{venues_groups}->{per_group};

	# separate the venues into groups of (whatever number we have designated)
	foreach my $v (@venues) {
		if (!defined $current_group || $group_count >= $group_max) {
			if (defined $current_group) {
				_add_venue_group( \@venues_groups, $current_group );
			}
			$current_group = [];
			$group_count = 0;
		}

		my $n = {
			label => $v->label,
			id => $v->id,
			name => $v->name,
			first_floorplan => $v->{first_floorplan}
		};
		push @{$current_group}, $n;
		$group_count++;
	}
	_add_venue_group( \@venues_groups, $current_group );
	
	#push @venues_groups, $current_group;
	#debug Dumper(\@venues_groups);
	my $display_cols = 12 / scalar(@venues_groups);
	var venues_group_css_class => sprintf('col-md-%s', $display_cols);
	var venues_groups => \@venues_groups;

	template 'index', {
		venues => \@venues,
		venue_groups => \@venues_groups,
	};
};

sub _add_venue_group {
	my ($venues_groups, $current_group) = @_;
	my $first = $$current_group[0];
	my $last = $$current_group[$#$current_group];
	my $heading_title = sprintf("%s - %s", uc(substr($first->{label}, 0, 1)), uc(substr($last->{label}, 0, 1)));
	my $collapse_in_indicator;

	if (scalar(@{$venues_groups}) == 0) {
		$collapse_in_indicator = 'in';
	}

	push @{$venues_groups}, {
		panel_body_id => "collapse" . $first->{name},
		panel_title => $heading_title,
		venues => $current_group,
		row_id => 'row' . $heading_title,
		collapse_in_indicator => $collapse_in_indicator,
	};
}

# route /
# Load the buildings, ordering by weight, then label
# Load the marketing copy text as specified by Iron Dukes staff
get '/venues' => sub {
	my @buildings = schema->resultset('Building')
		->search(
			{
				active => 1,
			},
			{
				order_by => [ { -desc => 'weight' }, 'label']
			}
		);
	grep {
		my $basedir = dirname( $0 );
		my $possible_image = $basedir . '/images/venues/' . $_->name . '.png';
		if (-e $basedir . '/images/venues/' . $_->name . '.png') {
			$_->{venue_image_url} = request->uri_base() . '/images/venues/' . $_->name . '.png';
			$_->{venue_image_url_wide} = request->uri_base() . '/images/venues/' . $_->name . '.png';
		} else {
			$_->{venue_image_url} = setting('ui')->{venues}->{images}->{default_url_desktop};
			$_->{venue_image_url_wide} = setting('ui')->{venues}->{images}->{default_url_wide};
		}
	} @buildings;

	push @{vars->{extrajs}}, 
		{ src => request->uri_base . '/javascripts/imagesloaded.pkgd.min.js' },
		{ src => request->uri_base . '/javascripts/masonry.pkgd.min.js' };

	push @{vars->{breadcrumbs}}, { title => 'Venues', url => '/venues' };
	push @{vars->{body_classes}}, 'masonry-aware';

  template 'venues', { buildings => \@buildings };
};

# route /spaces
get '/available-naming-opportunities' => sub {
	my @rooms = schema->resultset('Room')
		->search(
			{ 
				naming_opportunity => 1,
				naming_amount => { '>', 0 }
			},
			{
				join => { 'floorplan' => 'building' },
				order_by => [ { -desc => 'naming_amount'}, { -asc => 'me.label'} ],
			}
		);

	grep {
		my $label = $_->label;
		$label =~ s/\\n/<br \/>/g;
		$_->{filtered_label} = $label;

		my $naming_amount_formatted = format_price( $_->naming_amount, 0, '$' );
		my $gift = $naming_amount_formatted;
		$gift .= '<br />' . $_->naming_amount_desc unless !$_->naming_amount_desc;

		$_->{gift} = $gift;
		$_->{naming_amount_formatted} = format_price( $_->naming_amount, 0, '$' );
		$_->{display_title_or_label} = length($_->display_title) ? $_->display_title : $_->label;		
	} @rooms;

	push @{vars->{breadcrumbs}}, { title => 'All Available Naming Opportunities', url => '/available-naming-opportunties' };
	template 'available_naming_opportunties', { spaces => \@rooms };
};

get '/available-rooms' => sub {
	my @rooms = schema->resultset('Room')
		->search(
			undef,
			{
				join => { 'floorplan' => 'building' },
				order_by => [ { -asc => 'building.label'}, { -asc => 'floorplan.name' }, { -asc => 'me.label' } ],
			}
		);
	
	grep {
		$_->{naming_amount_formatted} = format_price( $_->naming_amount, 0, '$' );
		my @mockups = $_->search_related('room_mockups', undef);
		$_->{mockups} = \@mockups;
	} @rooms;

	template 'available_rooms', { spaces => \@rooms };
};

true;
